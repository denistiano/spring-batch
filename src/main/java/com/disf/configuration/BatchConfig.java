package com.disf.configuration;

import com.disf.mapper.UserFieldSetMapper;
import com.disf.model.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Created by denis on 6/9/16.
 */

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public ItemReader<User> reader() {
        //String csvFilename = "convertcsv";
        String csvFilename = "users";

        FlatFileItemReader fileReader = new FlatFileItemReader();
        fileReader.setResource(new FileSystemResource("csv/" + csvFilename + ".csv"));
        fileReader.setLineMapper(lineMapper());
        return fileReader;
    }

    @Bean
    public ItemProcessor<User, User> processor() {
        return item -> {
            //System.out.println("Before process: " + item.firstName + " " + item.lastName + " " + item.nickname + " " + item.city + "\n");
            item.firstName = item.firstName.toUpperCase();
            return item;
        };
    }

    @Bean
    public JdbcBatchItemWriter<User> writer() {
        //INFO: Working code for non-batch processing
        //return items -> {
        //    for(User user : items) {
        //        System.out.println("Processed user: " + user.firstName + " " + user.lastName + ", " + user.nickname);
        //        System.out.println("----------------------------");
        //    }
        //    System.out.print("|");
        //};
        JdbcBatchItemWriter<User> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(dataSource);
        jdbcBatchItemWriter.setAssertUpdates(true);
        jdbcBatchItemWriter.setItemPreparedStatementSetter((item, ps) -> {
            ps.setString(1, item.getNickname());
            ps.setString(2, item.getFirstName());
            ps.setString(3, item.getLastName());
            ps.setString(4, item.getCity());
        });
        jdbcBatchItemWriter.setSql("INSERT INTO users (" +
                "username, first_name, last_name, city) " +
                "VALUES (?, ?, ?, ? )");
        jdbcBatchItemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        return jdbcBatchItemWriter;
    }

    @Bean
    public Job userJob() {
        return jobBuilderFactory.get("userJob")
                .start(userStep())
                //.incrementer(parameters -> new RunIdIncrementer().getNext(parameters))
                .build();
    }

    @Bean
    public Step userStep() {
        return stepBuilderFactory.get("userStep")
                .allowStartIfComplete(true)
                .<User, User> chunk(2000) //2000, 3000, 5000
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    private DefaultLineMapper lineMapper() {
        DefaultLineMapper<User> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[] {"id", "nickname", "firstName", "lastName", "city"});
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new UserFieldSetMapper());
        return lineMapper;
    }

}
