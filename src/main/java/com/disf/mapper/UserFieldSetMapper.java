package com.disf.mapper;

import com.disf.model.User;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * Created by denis on 6/10/16.
 */
public class UserFieldSetMapper implements FieldSetMapper<User> {

    @Override
    public User mapFieldSet(FieldSet fieldSet) throws BindException {
        if(fieldSet == null) return null;

        User user = new User();
        user.id = fieldSet.readInt("id");
        user.nickname = fieldSet.readString("nickname");
        user.firstName = fieldSet.readString("firstName");
        user.lastName = fieldSet.readString("lastName");
        user.city = fieldSet.readString("city");

        return user;
    }
}
